from django.conf import settings
from django.contrib.auth.models import Group

from reimbursements.models import Request


def select_default_payer(request: Request) -> Group:
    """
    Given a newly filed request, select an appropriate payer for it.

    This implementation selects settings.DEFAULT_PAYER.
    """
    payer_name = settings.DEFAULT_PAYER
    return Group.objects.get(name=payer_name)


def select_payer_by_currency(request: Request) -> Group:
    """
    Given a newly filed request, select an appropriate payer for it.

    This implementation selects from settings.CURRENCY_PAYER by request
    currency.
    """
    payer_name = settings.DEFAULT_PAYER
    if request.currency in settings.CURRENCY_PAYER:
        payer_name = settings.CURRENCY_PAYER[request.currency]

    return Group.objects.get(name=payer_name)
