from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.mail import EmailMultiAlternatives
from django.template import Context, Engine

from debian_reimbursements.context_processors import meta


def render_email_template(template_name: str, part: str, context: Context) -> str:
    template_str = f"{{% extends '{template_name}' %}}\n"
    if part != "subject":
        template_str += "{% block subject %}{% endblock %}\n"
    if part != "body":
        template_str += "{% block body %}{% endblock %}\n"
    engine = Engine.get_default()
    template = engine.from_string(template_str)
    rendered = template.render(context).strip()
    if part == "subject" and settings.SITE_PROD_STATE != "production":
        rendered = f"{settings.SITE_PROD_STATE.upper()}: {rendered}"
    return rendered


def send_email(template_name: str, context: dict, to: list[AbstractUser]):
    to_addresses = [user.email for user in to]
    context.update(meta(None))
    ctx = Context(context)
    email = EmailMultiAlternatives(
        subject=render_email_template(template_name, "subject", ctx),
        body=render_email_template(template_name, "body", ctx),
        to=to_addresses,
    )
    email.send()
