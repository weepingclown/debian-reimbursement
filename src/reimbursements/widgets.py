from datetime import date

from django.forms.widgets import DateInput


class NativeDateInput(DateInput):
    """DateInput using an HTML5 native widget"""

    input_type = "date"

    def __init__(self, attrs=None, *args, **kwargs):
        if attrs:
            for key in ("min", "max"):
                if isinstance(attrs.get(key), date):
                    attrs[key] = attrs[key].isoformat()
        super().__init__(attrs, *args, **kwargs)
