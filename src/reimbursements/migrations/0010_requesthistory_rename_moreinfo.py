from django.db import migrations, models


def rename_not_approved(apps, schema_editor):
    RequestHistory = apps.get_model("reimbursements", "RequestHistory")
    RequestHistory.objects.filter(event="not_approved").update(
        event="moreinfo_approval"
    )
    RequestHistory.objects.filter(event="not_reimbursed").update(
        event="moreinfo_reimbursal"
    )


def reverse_rename_not_approved(apps, schema_editor):
    RequestHistory = apps.get_model("reimbursements", "RequestHistory")
    RequestHistory.objects.filter(event="moreinfo_approval").update(
        event="not_approved"
    )
    RequestHistory.objects.filter(event="moreinfo_reimbursal").update(
        event="not_reimbursed"
    )


class Migration(migrations.Migration):
    dependencies = [
        ("reimbursements", "0009_uniqueconstraint"),
    ]

    operations = [
        migrations.AlterField(
            model_name="requesthistory",
            name="event",
            field=models.CharField(
                choices=[
                    ("created", "Created"),
                    ("updated", "Updated"),
                    ("proposed", "Proposed"),
                    ("approved", "Approved"),
                    ("rejected", "Rejected"),
                    ("moreinfo_approval", "Information requested for approval"),
                    ("withdrawn", "Withdrawn"),
                    ("submitted", "Submitted"),
                    ("reimbursed", "Reimbursed"),
                    (
                        "moreinfo_reimbursal",
                        "Information requested for reimbursement",
                    ),
                    ("note", "Note"),
                    ("receipt_added", "Receipt Added"),
                    ("receipt_deleted", "Receipt Deleted"),
                    ("receipt_updated", "Receipt Updated"),
                    ("budget_increase", "Returned for budget increase"),
                ],
                max_length=20,
            ),
        ),
        migrations.RunPython(rename_not_approved, reverse_rename_not_approved),
    ]
