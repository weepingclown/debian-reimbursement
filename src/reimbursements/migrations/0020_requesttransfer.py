import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("reimbursements", "0019_alter_request_requester"),
    ]

    operations = [
        migrations.AlterField(
            model_name="requesthistory",
            name="event",
            field=models.CharField(
                choices=[
                    ("created", "Created"),
                    ("updated", "Updated"),
                    ("proposed", "Proposed"),
                    ("approved", "Approved"),
                    ("rejected", "Rejected"),
                    ("moreinfo_approval", "Information requested for approval"),
                    ("withdrawn", "Withdrawn"),
                    ("submitted", "Submitted"),
                    ("reimbursed", "Reimbursed"),
                    ("moreinfo_reimbursal", "Information requested for reimbursement"),
                    ("note", "Note"),
                    ("receipt_added", "Receipt Added"),
                    ("receipt_deleted", "Receipt Deleted"),
                    ("receipt_updated", "Receipt Updated"),
                    ("budget_increase", "Returned for budget increase"),
                    ("currency_change", "Changed request currency"),
                    ("payer_change", "Changed request payer"),
                    ("transfer_initiated", "Generated a transfer token"),
                    ("transferred", "Transferred request"),
                ],
                max_length=20,
            ),
        ),
        migrations.CreateModel(
            name="RequestTransfer",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("expires", models.DateTimeField()),
                ("token", models.CharField(max_length=20, unique=True)),
                (
                    "request",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="transfer",
                        to="reimbursements.request",
                    ),
                ),
            ],
        ),
    ]
