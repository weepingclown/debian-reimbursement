# Generated by Django 4.1.7 on 2023-10-07 19:02

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("reimbursements", "0006_rtticket"),
    ]

    operations = [
        migrations.CreateModel(
            name="ReimbursementCost",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("description", models.TextField()),
                ("amount", models.DecimalField(decimal_places=2, max_digits=15)),
                ("currency", models.CharField(default="USD", max_length=3)),
                (
                    "request",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="reimbursement_cost",
                        to="reimbursements.request",
                    ),
                ),
            ],
        ),
    ]
