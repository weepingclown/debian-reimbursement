# Generated by Django 4.1.7 on 2024-03-13 15:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("reimbursements", "0011_request_visible"),
    ]

    operations = [
        migrations.AlterField(
            model_name="requesthistory",
            name="event",
            field=models.CharField(
                choices=[
                    ("created", "Created"),
                    ("updated", "Updated"),
                    ("proposed", "Proposed"),
                    ("approved", "Approved"),
                    ("rejected", "Rejected"),
                    ("moreinfo_approval", "Information requested for approval"),
                    ("withdrawn", "Withdrawn"),
                    ("submitted", "Submitted"),
                    ("reimbursed", "Reimbursed"),
                    ("moreinfo_reimbursal", "Information requested for reimbursement"),
                    ("note", "Note"),
                    ("receipt_added", "Receipt Added"),
                    ("receipt_deleted", "Receipt Deleted"),
                    ("receipt_updated", "Receipt Updated"),
                    ("budget_increase", "Returned for budget increase"),
                    ("currency_change", "Changed request currency"),
                ],
                max_length=20,
            ),
        ),
    ]
