# Generated by Django 5.0.3 on 2024-05-09 19:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("reimbursements", "0013_receipt_custom_conversions"),
    ]

    operations = [
        migrations.AlterField(
            model_name="requesthistory",
            name="event",
            field=models.CharField(
                choices=[
                    ("created", "Created"),
                    ("updated", "Updated"),
                    ("proposed", "Proposed"),
                    ("approved", "Approved"),
                    ("rejected", "Rejected"),
                    ("moreinfo_approval", "Information requested for approval"),
                    ("withdrawn", "Withdrawn"),
                    ("submitted", "Submitted"),
                    ("reimbursed", "Reimbursed"),
                    ("moreinfo_reimbursal", "Information requested for reimbursement"),
                    ("note", "Note"),
                    ("receipt_added", "Receipt Added"),
                    ("receipt_deleted", "Receipt Deleted"),
                    ("receipt_updated", "Receipt Updated"),
                    ("budget_increase", "Returned for budget increase"),
                    ("currency_change", "Changed request currency"),
                    ("payer_change", "Changed request payer"),
                ],
                max_length=20,
            ),
        ),
    ]
