{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request payer reassigned: {{ subject }}
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with old_payer_group_name=old_payer_group.name new_payer_group=object.payer_group.name approver_group=object.approver_group.name requester_name=object.requester_name request_url=object.get_fully_qualified_url subject=object.subject actor_name=actor.get_full_name %}
Dear {{ old_payer_group_name }} and {{ approver_group }} members,

The reimbursement request by {{ requester_name }} for {{ subject }}
has been reassigned from {{ old_payer_group_name }} to {{ new_payer_group }}.
You can review the full request here:
{{ request_url }}

Thank you,

The {{ SITE_NAME }} system on behalf of {{ actor_name }}.
{% endblocktrans %}
{% endblock %}
