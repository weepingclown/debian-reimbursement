{% load i18n %}
{% load currency_format %}
{% blocktrans with request_id=request.id request_url=request.get_fully_qualified_url request_description=request.description reimbursed_total=request.reimbursed_total|currency %}
{{ SITE_NAME }} Request {{ request_id }} Reimbursed
{{ request_url }}
Details:
{{ details }}

Total Reimbursed: {{ reimbursed_total }}
From:
{% endblocktrans %}{% for type in request.summarize_by_type.values %}{% if not type.special %}
* {{ type.name }}: {{ type.qualified_reimbursed|currency }}{% endif %}{% endfor %}{% blocktrans %}

This RT request is for auditing purposes only.
Please discuss this request on {{ SITE_NAME }} if possible, not RT.
{% endblocktrans %}
