{% load i18n %}
{% load currency_format %}
{% blocktrans with request_id=request.id request_url=request.get_fully_qualified_url request_description=request.description %}
{{ SITE_NAME }} Request {{ request_id }} Withdrawn
{{ request_url }}
Details:
{{ details }}

This RT request is for auditing purposes only.
Please discuss this request on {{ SITE_NAME }} if possible, not RT.
{% endblocktrans %}
