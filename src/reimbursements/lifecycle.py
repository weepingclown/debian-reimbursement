from functools import partial, wraps
from logging import getLogger

from django.db import transaction

from historical_currencies.exchange import exchange, latest_rate

from reimbursements.email import send_email
from reimbursements.integrations.rt import RT
from reimbursements.models import Request, RequestHistory

log = getLogger(__name__)


def state_change(old_state, new_state):
    def decorator(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            with transaction.atomic():
                assert request.state == old_state
                request.state = new_state
                request.save()
                return function(request, *args, **kwargs)

        return wrapper

    return decorator


def request_created(request, actor):
    """A new request has been created by actor"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.CREATED,
    ).save()


def request_commented(request, actor, details):
    """An actor leaves a comment on a request"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.NOTE,
        details=details,
    ).save()


def request_currency_changed(request, actor, details, new_currency):
    """An actor changes the currency of an already approved request."""
    assert request.state in (Request.States.APPROVED, Request.States.PENDING_PAYMENT)
    old_currency = request.currency
    conversion_date = request.created.date()
    with transaction.atomic():
        for line in request.lines.all():
            line.amount = exchange(
                line.amount, old_currency, new_currency, conversion_date
            )
            line.save()
        request.currency = new_currency
        request.save()
        for receipt in request.receipts.all():
            for line in receipt.lines.all():
                if receipt.custom_conversion:
                    line.converted_amount = exchange(
                        line.converted_amount, old_currency, new_currency, line.date
                    )
                else:
                    line.clean()
                line.save()
    rate_date, rate = latest_rate(old_currency, new_currency, conversion_date)
    details = f"Change from {old_currency} to {new_currency} at {rate}\n{details}"
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.CURRENCY_CHANGE,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    transaction.on_commit(request.generate_pdf_bundle)


def request_payer_changed(request, actor, details, new_payer):
    """An actor changes the payer of a request."""
    assert request.state not in (Request.States.DELETED, Request.States.PAID)
    with transaction.atomic():
        RT.update_ticket_if_exists(
            request,
            "reimbursements/rt/payer_changed_body.txt",
            {"details": details},
            close=True,
        )

        old_payer = request.payer_group
        request.payer_group = new_payer
        request.save()

        details = f"Change payer from {old_payer.name} to {new_payer.name}\n{details}"
        RequestHistory(
            request=request,
            actor=actor,
            event=RequestHistory.Events.PAYER_CHANGE,
            details=details,
        ).save()
        if request.state == Request.States.PENDING_PAYMENT:
            transaction.on_commit(request.generate_pdf_bundle, robust=True)
            transaction.on_commit(
                partial(
                    send_email,
                    "reimbursements/email/request_pending_payment.txt",
                    {"object": request},
                    request.payer_group.user_set.all(),
                ),
                robust=True,
            )
            transaction.on_commit(
                partial(
                    send_email,
                    "reimbursements/email/request_changed_payer.txt",
                    {"object": request, "old_payer_group": old_payer, "actor": actor},
                    old_payer.user_set.all() | request.approver_group.user_set.all(),
                ),
                robust=True,
            )
            if RT.has_instance_for_group(new_payer):
                transaction.on_commit(
                    partial(
                        RT.create_or_update_ticket,
                        request,
                        "reimbursements/rt/ticket_body.txt",
                        reopen=True,
                    ),
                    robust=True,
                )


@state_change(Request.States.DRAFT, Request.States.PENDING_APPROVAL)
def request_proposed(request, actor, details):
    """An existing request has been submitted for review by actor"""
    request.visible = True
    request.save()
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.PROPOSED,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    if request.requester:
        send_email(
            "reimbursements/email/request_ack.txt",
            {"object": request},
            [request.requester],
        )
    send_email(
        "reimbursements/email/request_received.txt",
        {"object": request},
        request.approver_group.user_set.all(),
    )
    transaction.on_commit(request.generate_pdf_bundle)


@state_change(Request.States.APPROVED, Request.States.DRAFT)
def request_proposed_increase(request, actor, details):
    """Return an existing approved request to draft status, for a budget increase"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.BUDGET_INCREASE,
        details=details,
    ).save()


@state_change(Request.States.APPROVED, Request.States.PENDING_PAYMENT)
def request_submitted(request, actor):
    """An request has been submitted for reimbursement by actor"""
    assert request.requester is not None
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.SUBMITTED,
        budget=request.summarize_by_type_dict(),
    ).save()
    send_email(
        "reimbursements/email/request_submitted.txt",
        {"object": request},
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_pending_payment.txt",
        {"object": request},
        request.payer_group.user_set.all(),
    )
    transaction.on_commit(request.generate_pdf_bundle)
    if RT.has_instance_for_group(request.payer_group):
        RT.create_or_update_ticket(request, "reimbursements/rt/ticket_body.txt")


@state_change(Request.States.PENDING_PAYMENT, Request.States.PAID)
def request_reimbursed(request, actor, details):
    """An request has been reimbursed by actor"""
    assert request.requester is not None
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.REIMBURSED,
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_reimbursed.txt",
        {"object": request},
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_reimbursement_recorded.txt",
        {
            "actor": actor,
            "object": request,
        },
        request.payer_group.user_set.all(),
    )
    transaction.on_commit(request.generate_pdf_bundle)
    RT.update_ticket_if_exists(
        request,
        "reimbursements/rt/reimbursed_body.txt",
        {"details": details},
        close=True,
    )


def request_updated(request, actor):
    """An existing request has been updated by actor"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.UPDATED,
        budget=request.summarize_by_type_dict(),
    ).save()


@state_change(Request.States.PENDING_APPROVAL, Request.States.APPROVED)
def request_approved(request, actor, details):
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.APPROVED,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    if request.requester:
        send_email(
            "reimbursements/email/request_approved.txt",
            {
                "details": details,
                "object": request,
            },
            [request.requester],
        )
    send_email(
        "reimbursements/email/request_approved_ack.txt",
        {
            "actor": actor,
            "details": details,
            "object": request,
        },
        request.approver_group.user_set.all(),
    )


@state_change(Request.States.PENDING_APPROVAL, Request.States.DELETED)
def request_rejected(request, actor, details):
    assert request.requester is not None
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.REJECTED,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_rejected.txt",
        {
            "details": details,
            "object": request,
        },
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_rejected_ack.txt",
        {
            "actor": actor,
            "details": details,
            "object": request,
        },
        request.approver_group.user_set.all(),
    )
    transaction.on_commit(request.generate_pdf_bundle)


@state_change(Request.States.PENDING_APPROVAL, Request.States.DRAFT)
def request_returned_for_more_info(request, actor, details):
    assert request.requester is not None
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.MOREINFO_APPROVAL,
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_moreinfo.txt",
        {
            "details": details,
            "object": request,
        },
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_moreinfo_ack.txt",
        {
            "actor": actor,
            "details": details,
            "object": request,
        },
        request.approver_group.user_set.all(),
    )


@state_change(Request.States.DRAFT, Request.States.DELETED)
def request_draft_deleted(request, actor, details):
    """A draft request has been deleted by actor"""
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.WITHDRAWN,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    if request.requester is not None:
        send_email(
            "reimbursements/email/request_deleted.txt",
            {"object": request, "details": details},
            [request.requester],
        )
    # It may have been proposed and/or approved in the past
    if request.history.filter(event=RequestHistory.Events.PROPOSED).exists():
        send_email(
            "reimbursements/email/request_withdrawn_approver.txt",
            {"object": request, "details": details},
            request.approver_group.user_set.all(),
        )
    if request.history.filter(event=RequestHistory.Events.SUBMITTED).exists():
        send_email(
            "reimbursements/email/request_withdrawn_payer.txt",
            {"object": request, "details": details},
            request.payer_group.user_set.all(),
        )
    RT.update_ticket_if_exists(
        request,
        "reimbursements/rt/request_withdrawn_body.txt",
        {"details": details},
        close=True,
    )


@state_change(Request.States.APPROVED, Request.States.DELETED)
def request_withdrawn(request, actor, details):
    """An approved request has been deleted by actor"""
    assert request.requester is not None
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.WITHDRAWN,
        budget=request.summarize_by_type_dict(),
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_deleted.txt",
        {"object": request, "details": details},
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_withdrawn_approver.txt",
        {"object": request, "details": details},
        request.approver_group.user_set.all(),
    )
    if request.history.filter(event=RequestHistory.Events.SUBMITTED).exists():
        send_email(
            "reimbursements/email/request_withdrawn_payer.txt",
            {"object": request, "details": details},
            request.payer_group.user_set.all(),
        )
    transaction.on_commit(request.generate_pdf_bundle)
    RT.update_ticket_if_exists(
        request,
        "reimbursements/rt/request_withdrawn_body.txt",
        {"details": details},
        close=True,
    )


@state_change(Request.States.PENDING_PAYMENT, Request.States.APPROVED)
def reimbursement_returned_for_more_info(request, actor, details):
    assert request.requester is not None
    RequestHistory(
        request=request,
        actor=actor,
        event=RequestHistory.Events.MOREINFO_REIMBURSAL,
        details=details,
    ).save()
    send_email(
        "reimbursements/email/request_reimbursement_moreinfo.txt",
        {
            "details": details,
            "object": request,
        },
        [request.requester],
    )
    send_email(
        "reimbursements/email/request_reimbursement_moreinfo_ack.txt",
        {
            "actor": actor,
            "details": details,
            "object": request,
        },
        request.payer_group.user_set.all(),
    )
    transaction.on_commit(request.generate_pdf_bundle)
    RT.update_ticket_if_exists(
        request,
        "reimbursements/rt/reimbursement_moreinfo_body.txt",
        {"details": details},
    )


def receipt_created(receipt, actor):
    try:
        receipt.generate_thumbnails()
    except Exception:
        log.info(
            "Failed to generate thumbnail for receipt %s", receipt.id, exc_info=True
        )
    RequestHistory(
        request=receipt.request,
        actor=actor,
        event=RequestHistory.Events.RECEIPT_ADDED,
    ).save()


def receipt_updated(receipt, actor):
    try:
        receipt.generate_thumbnails()
    except Exception:
        log.info(
            "Failed to generate thumbnail for receipt %s", receipt.id, exc_info=True
        )
    RequestHistory(
        request=receipt.request,
        actor=actor,
        event=RequestHistory.Events.RECEIPT_UPDATED,
    ).save()


def receipt_deleted(receipt, actor):
    RequestHistory(
        request=receipt.request,
        actor=actor,
        event=RequestHistory.Events.RECEIPT_DELETED,
    ).save()


def request_transfer_initiate(request, actor, expire_days=30):
    assert not hasattr(request, "transfer")
    with transaction.atomic():
        request.initiate_transfer()
        RequestHistory(
            request=request,
            actor=actor,
            event=RequestHistory.Events.TRANSFER_INITIATED,
        ).save()


def request_transfer_complete(request, actor):
    old_requester = request.requester
    old_requester_name = request.requester_name
    with transaction.atomic():
        request.transfer.delete()
        request.requester = actor
        request.save()

        RequestHistory(
            request=request,
            actor=actor,
            event=RequestHistory.Events.TRANSFERRED,
        ).save()

        recipients = [actor]
        if old_requester:
            recipients.append(old_requester)

        send_email(
            "reimbursements/email/request_transferred.txt",
            {
                "actor": actor,
                "object": request,
                "old_requester": old_requester,
                "old_requester_name": old_requester_name,
            },
            recipients,
        )
