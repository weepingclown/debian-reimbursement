from datetime import date
from decimal import Decimal
from typing import Any

from django.test import TestCase

from reimbursements.forms.receiptline import ReceiptLineForm
from reimbursements.models import Receipt
from reimbursements.tests.helpers import RequestHelpers


class TestReceiptLineForm(TestCase, RequestHelpers):
    def create_receipt_line_form(
        self,
        receipt: Receipt,
        extra_initial: dict[str, Any] | None = None,
        extra_data: dict[str, Any] | None = None,
    ) -> ReceiptLineForm:
        initial = {
            "currency": "USD",
        }
        if extra_initial is not None:
            initial.update(extra_initial)
        data = {
            "receipt": receipt.id,
            "date": date(2023, 1, 21),
            "description": "Test Hotel",
            "amount": Decimal(59),
            "currency": "EUR",
            "expense_type": self.accomm.id,
            "converted_amount": "",
        }
        if extra_data is not None:
            data.update(extra_data)
        return ReceiptLineForm(initial=initial, data=data)

    def test_clean_leaves_converts_amount_when_not_customized(self):
        self.create_request()
        receipt = self.create_receipt()
        form = self.create_receipt_line_form(
            receipt,
            extra_data={
                "converted_amount": Decimal(42),
            },
        )
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data["converted_amount"], Decimal("64.18"))

    def test_clean_converts_amount_when_converted_amount_empty(self):
        self.create_request()
        receipt = self.create_receipt(custom_conversion=True)
        self.create_exchange_rate()
        form = self.create_receipt_line_form(
            receipt,
            extra_data={
                "converted_amount": "",
            },
        )
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data["converted_amount"], Decimal("64.18"))

    def test_clean_leaves_converted_amount_intact_when_customized(self):
        self.create_request()
        receipt = self.create_receipt(custom_conversion=True)
        form = self.create_receipt_line_form(
            receipt,
            extra_data={
                "converted_amount": Decimal(42),
            },
        )
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data["converted_amount"], Decimal("42"))

    def test_clean_reports_validation_error_without_rates(self):
        self.create_request()
        receipt = self.create_receipt()
        form = self.create_receipt_line_form(
            receipt,
            extra_data={
                "converted_amount": "",
                "currency": "GBP",
            },
        )

        form.full_clean()
        self.assertFalse(form.is_valid())
        errors = form.errors.as_data()

        self.assertIn("currency", errors)
        self.assertEqual(len(errors["currency"]), 1)
        inner_error = errors["currency"][0]
        self.assertEqual(
            inner_error.messages,
            [
                "No exchange rate available for GBP to USD for 2023-01-21. "
                "Please contact an admin to get it imported."
            ],
        )

    def test_clean_handles_noop_conversions(self):
        self.create_request()
        receipt = self.create_receipt()
        form = self.create_receipt_line_form(
            receipt,
            extra_data={
                "currency": "USD",
            },
        )
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data["converted_amount"], Decimal("59"))
