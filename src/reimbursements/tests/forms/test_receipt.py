from typing import Any

from django.test import TestCase

from reimbursements.forms.receipt import ReceiptForm
from reimbursements.models import Receipt, Request
from reimbursements.tests.helpers import RequestHelpers


class TestReceiptForm(TestCase, RequestHelpers):
    def create_receipt_form(
        self,
        request: Request | None = None,
        extra_initial: dict[str, Any] | None = None,
        extra_data: dict[str, Any] | None = None,
        extra_files: dict[str, Any] | None = None,
        instance: Receipt | None = None,
        random_image: bool = True,
    ) -> ReceiptForm:
        initial = {}
        if extra_initial is not None:
            initial.update(extra_initial)
        data = {
            "description": "A Test Receipt",
            "custom_conversion": False,
        }
        if extra_data is not None:
            data.update(extra_data)

        if instance:
            files = {}
        else:
            files = {
                "file": self.create_receipt_image(random=random_image),
            }
        if extra_files is not None:
            files.update(extra_files)
        form = ReceiptForm(initial=initial, data=data, files=files, instance=instance)
        form.request = request or self.request
        return form

    def test_clean_executes(self):
        self.create_request()
        form = self.create_receipt_form()
        self.assertTrue(form.is_valid())

    def test_duplicate_files_rejected(self):
        self.create_request()
        self.create_receipt(random_image=False)
        form = self.create_receipt_form(random_image=False)

        self.assertFalse(form.is_valid())
        errors = form.errors.as_data()

        self.assertIn("file", errors)
        self.assertEqual(len(errors["file"]), 1)
        inner_error = errors["file"][0]
        self.assertEqual(
            inner_error.messages,
            ["This file is already used as a receipt on this request"],
        )

    def test_duplicate_files_permitted_across_requests(self):
        self.create_request()
        self.create_receipt(random_image=False)
        self.create_request()
        form = self.create_receipt_form(random_image=False)
        self.assertTrue(form.is_valid())

    def test_noop_edit(self):
        self.create_request()
        receipt = self.create_receipt()
        form = self.create_receipt_form(instance=receipt)
        self.assertTrue(form.is_valid())
