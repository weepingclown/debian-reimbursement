from django.contrib.auth.models import Group
from django.test import SimpleTestCase

from reimbursements.models.rt import RTTicket


class TestRTTicket(SimpleTestCase):
    def test_url(self):
        ticket = RTTicket(payer_group=Group(name="Payers"), ticket_id=42)
        with self.settings(RT_INSTANCES={"Payers": {"url": "https://rt.example.com"}}):
            self.assertEqual(
                ticket.url, "https://rt.example.com/Ticket/Display.html?id=42"
            )
