from decimal import Decimal

from django.test import SimpleTestCase, TestCase

from reimbursements.models.receipt import Receipt
from reimbursements.models.request import Request

from reimbursements.tests.helpers import RequestHelpers


class TestReceiptSimpleMethods(SimpleTestCase):
    def test_subject(self):
        receipt = Receipt(description="Line 1\nLine 2\nLine 3")
        self.assertEqual(receipt.subject, "Line 1")

    def test_subject_strips(self):
        receipt = Receipt(description=" Line 1\t\nLine 2\nLine 3")
        self.assertEqual(receipt.subject, "Line 1")

    def test_subject_empty(self):
        receipt = Receipt(description="")
        self.assertEqual(receipt.subject, "")

    def test_str(self):
        receipt = Receipt(
            request=Request(id=2, description="I Need Stuff"),
            description="Receipt for stuff",
        )
        self.assertEqual(str(receipt), "Request 2: I Need Stuff: Receipt for stuff")


class TestReceipt(TestCase, RequestHelpers):
    def test_total(self):
        self.create_request()
        receipt = self.create_receipt()
        self.assertEqual(receipt.total, (Decimal(0), "USD"))
        self.create_exchange_rate()
        self.create_receipt_line(receipt)
        self.assertEqual(receipt.total, (Decimal("64.18"), "USD"))
        self.create_receipt_line(receipt)
        self.assertEqual(receipt.total, (Decimal("128.36"), "USD"))
