from datetime import datetime
from django.test import SimpleTestCase, TestCase

from reimbursements.models.request import Request
from reimbursements.models.transfer import RequestTransfer
from reimbursements.tests.helpers import RequestHelpers


class TestRequestTransfer(SimpleTestCase):
    def test_get_absolute_url(self):
        request = Request()
        transfer = RequestTransfer(
            request=request,
            expires=datetime(2023, 1, 1),
            token="abc1234",
        )
        self.assertEqual(
            transfer.get_absolute_url(),
            "/requests/transfer/abc1234",
        )

    def test_get_fully_qualified_url(self):
        request = Request()
        transfer = RequestTransfer(
            request=request,
            expires=datetime(2023, 1, 1),
            token="abc1234",
        )
        with self.settings(SITE_URL="https://reimbursements.example.com/"):
            self.assertEqual(
                transfer.get_fully_qualified_url(),
                "https://reimbursements.example.com/requests/transfer/abc1234",
            )


class TestRequestTransferManager(TestCase, RequestHelpers):
    def test_get_by_token(self):
        request = self.create_request()
        transfer = request.initiate_transfer()

        self.assertEqual(RequestTransfer.objects.get_by_token(transfer.token), transfer)

    def test_get_by_token_expired(self):
        request = self.create_request()
        transfer = request.initiate_transfer(expire_days=-1)

        with self.assertRaises(RequestTransfer.DoesNotExist):
            RequestTransfer.objects.get_by_token(transfer.token)
