from datetime import date
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.test import SimpleTestCase, TestCase

from historical_currencies.models import ExchangeRate

from reimbursements.models.receipt import Receipt
from reimbursements.models.receiptline import ReceiptLine
from reimbursements.models.request import Request
from reimbursements.models.types import ExpenseType

from reimbursements.tests.helpers import RequestHelpers


class TestReceiptLineSimpleMethods(SimpleTestCase):
    def test_qualified_amount(self):
        line = ReceiptLine(amount=Decimal(12), currency="USD")
        self.assertEqual(line.qualified_amount, (Decimal(12), "USD"))

    def test_qualified_converted_amount(self):
        line = ReceiptLine(
            receipt=Receipt(request=Request(currency="USD")),
            converted_amount=Decimal(13),
            currency="GBP",
        )
        self.assertEqual(line.qualified_converted_amount, (Decimal(13), "USD"))

    def test_clean_without_request(self):
        line = ReceiptLine(
            receipt=Receipt(),
            amount=Decimal(12),
            currency="GBP",
        )
        line.clean()

    def test_clean_without_receipt(self):
        line = ReceiptLine(
            amount=Decimal(12),
            currency="GBP",
        )
        line.clean()


class TestReceiptLine(TestCase, RequestHelpers):
    def test_clean_converts_amount(self):
        self.create_request()
        self.create_exchange_rate()
        line = self.create_receipt_line(self.create_receipt())
        line.converted_amount = Decimal(0)
        line.clean()
        self.assertEqual(line.converted_amount, Decimal("64.18"))

    def test_clean_doesnt_convert_customized_amount(self):
        self.create_request()
        self.create_exchange_rate()
        line = self.create_receipt_line(self.create_receipt(custom_conversion=True))
        line.converted_amount = Decimal(1)
        line.clean()
        self.assertEqual(line.converted_amount, Decimal(1))

    def test_clean_reports_validation_error_without_rates(self):
        self.create_request()
        self.create_exchange_rate()
        line = self.create_receipt_line(self.create_receipt())

        line.currency = "GBP"
        with self.assertRaises(ValidationError) as cm:
            line.clean()

        self.assertIn("currency", cm.exception.error_dict)
        self.assertEqual(len(cm.exception.error_dict["currency"]), 1)
        inner_error = cm.exception.error_dict["currency"][0]
        self.assertEqual(
            inner_error.messages,
            [
                "No exchange rate available for GBP to USD for 2023-01-21. "
                "Please contact an admin to get it imported."
            ],
        )

    def test_clean_handles_same_currency(self):
        self.create_request()
        line = self.create_receipt_line(self.create_receipt(), currency="USD")
        line.clean()

    def test_clean_rejects_invalid_expense_type(self):
        self.create_request()
        unrelated = ExpenseType.objects.create(name="Unrelated")
        receipt = self.create_receipt()
        self.create_exchange_rate()
        line = self.create_receipt_line(receipt, expense_type=unrelated)

        with self.assertRaises(ValidationError) as cm:
            line.clean()

        self.assertIn("expense_type", cm.exception.error_dict)
        self.assertEqual(len(cm.exception.error_dict["expense_type"]), 1)
        inner_error = cm.exception.error_dict["expense_type"][0]
        self.assertEqual(
            inner_error.messages,
            ["Unrelated is not a valid expense type for Travel requests"],
        )


class TestUpdateReceiptConversions(TestCase, RequestHelpers):
    def test_updates_conversions(self):
        self.create_request()
        receipt = self.create_receipt()
        self.create_exchange_rate()
        line = self.create_receipt_line(receipt)
        self.assertEqual(line.converted_amount, Decimal("64.18"))

        # Change request currency
        ExchangeRate.objects.create(
            date=date(2023, 1, 21),
            base_currency="USD",
            currency="GBP",
            rate=Decimal("0.80717"),
        )
        self.request.currency = "GBP"
        self.request.save()
        line = receipt.lines.first()
        self.assertEqual(line.converted_amount, Decimal("51.80"))

    def test_doesnt_update_customized_conversions(self):
        self.create_request()
        receipt = self.create_receipt(custom_conversion=True)
        self.create_exchange_rate()
        line = self.create_receipt_line(receipt)
        self.assertEqual(line.converted_amount, Decimal("64.18"))

        # Change request currency
        ExchangeRate.objects.create(
            date=date(2023, 1, 21),
            base_currency="USD",
            currency="GBP",
            rate=Decimal("0.80717"),
        )
        self.request.currency = "GBP"
        self.request.save()
        line = receipt.lines.first()
        self.assertEqual(line.converted_amount, Decimal("64.18"))
