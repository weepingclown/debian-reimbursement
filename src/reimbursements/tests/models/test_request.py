from datetime import datetime
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.test import SimpleTestCase, TestCase

from reimbursements.models.request import Request
from reimbursements.models.transfer import RequestTransfer
from reimbursements.models.types import ExpenseType

from reimbursements.tests.helpers import RequestHelpers


class TestRequestSimpleMethods(SimpleTestCase):
    def test_subject_multiline(self):
        request = Request(description="Line 1\nLine 2\nLine 3")
        self.assertEqual(request.subject, "Line 1")

    def test_subject_oneline(self):
        request = Request(description="Line 1")
        self.assertEqual(request.subject, "Line 1")

    def test_subject_empty(self):
        request = Request(description="")
        self.assertEqual(request.subject, "")

    def test_str(self):
        request = Request(id=42, description="Line 1\nLine 2")
        self.assertEqual(str(request), "Request 42: Line 1")


class TestEmptyRequest(TestCase, RequestHelpers):
    def setUp(self):
        self.create_request()

    def test_total(self):
        self.assertEqual(self.request.total, (Decimal(0), "USD"))

    def test_receipts_total(self):
        self.assertEqual(self.request.receipts_total, (Decimal(0), "USD"))

    def test_reimbursed_total(self):
        self.assertEqual(self.request.reimbursed_total, (Decimal(0), "USD"))

    def test_approved_budget_limit(self):
        self.assertEqual(self.request.approved_budget_limit, (Decimal(0), "USD"))

    def test_total_reimburseable(self):
        self.assertEqual(self.request.total_reimburseable, (Decimal(0), "USD"))

    def test_within_budget(self):
        self.assertEqual(self.request.within_budget, True)

    def test_over_budget(self):
        self.assertEqual(self.request.over_budget, False)

    def test_created(self):
        self.assertIsInstance(self.request.created, datetime)

    def test_updated(self):
        self.assertEqual(self.request.created, self.request.updated)

    def test_absolute_url(self):
        self.assertEqual(
            self.request.get_absolute_url(), f"/requests/{self.request.id}"
        )

    def test_fully_qualified_url(self):
        with self.settings(SITE_URL="https://reimbursements.example.com/"):
            self.assertEqual(
                self.request.get_fully_qualified_url(),
                f"https://reimbursements.example.com/requests/{self.request.id}",
            )

    def test_summarize_by_type(self):
        summary = self.request.summarize_by_type()
        self.assertIsInstance(summary, dict)
        self.assertEqual(list(summary.keys()), ["total"])
        total = summary["total"]
        self.assertTrue(total.special)
        self.assertEqual(total.name, "Total")
        self.assertEqual(total.currency, "USD")
        self.assertEqual(total.spent, Decimal(0))
        self.assertEqual(total.reimbursed, Decimal(0))

    def test_summarize_by_type_dict(self):
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 0.0,
                    "spent": 0.0,
                },
            },
        )

    def test_requester_name_named(self):
        self.user.first_name = "Fake"
        self.user.last_name = "User"
        self.user.save()
        self.assertEqual(self.request.requester_name, "Fake User")

    def test_requester_name_fallback(self):
        self.assertEqual(self.request.requester_name, "user")

    def test_requester_name_null(self):
        self.request.requester = None
        self.assertEqual(self.request.requester_name, "Currently Unclaimed")

    def test_user_access_requester(self):
        self.assertEqual(self.request.user_access(self.user), {"requester": True})

    def test_user_access_approver_before_submission(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        user2.groups.set([self.approvers])
        self.assertEqual(self.request.user_access(user2), {})

    def test_user_access_approver(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        user2.groups.set([self.approvers])
        self.request.visible = True
        self.assertEqual(self.request.user_access(user2), {"approver": True})

    def test_user_access_payer_before_submission(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        user2.groups.set([self.payers])
        self.assertEqual(self.request.user_access(user2), {})

    def test_user_access_payer(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        user2.groups.set([self.payers])
        self.request.visible = True
        self.assertEqual(self.request.user_access(user2), {"payer": True})

    def test_user_access_unrelated(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        self.assertEqual(self.request.user_access(user2), {})

    def test_initiate_transfer(self):
        transfer = self.request.initiate_transfer()
        self.assertIsInstance(transfer, RequestTransfer)

    def test_pretty_bank_details(self):
        self.assertIsNone(self.request.pretty_bank_details)


class TestNonTrivialRequests(TestCase, RequestHelpers):
    def test_request_lines(self):
        self.create_request()
        self.create_request_line()
        self.assertEqual(self.request.total, (Decimal(100), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 0.0,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 0.0,
                },
            },
        )
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(self.request.approved_budget_limit, (Decimal(105), "USD"))

    def test_receipt_lines(self):
        self.create_request()
        self.create_request_line()
        self.create_exchange_rate()
        self.create_receipt_line(self.create_receipt())
        self.assertEqual(self.request.receipts_total, (Decimal("64.18"), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
            },
        )
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(
                self.request.total_reimburseable, (Decimal("64.18"), "USD")
            )
            self.assertTrue(self.request.within_budget)

    def test_receipt_lines_over_budget(self):
        self.create_request()
        self.create_request_line()
        receipt = self.create_receipt()
        self.create_receipt_line(receipt)
        self.create_receipt_line(receipt)
        self.assertEqual(self.request.receipts_total, (Decimal("128.36"), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 128.36,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 128.36,
                },
            },
        )
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(self.request.total_reimburseable, (Decimal(105), "USD"))
            self.assertTrue(self.request.over_budget)

    def test_receipt_lines_across_types(self):
        self.create_request()
        self.create_request_line()
        receipt = self.create_receipt()
        self.create_receipt_line(receipt)
        trains = ExpenseType.objects.create(name="Train Tickets")
        self.create_receipt_line(receipt, expense_type=trains)
        self.assertEqual(self.request.receipts_total, (Decimal("128.36"), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
                "Train Tickets": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 0.0,
                    "spent": 64.18,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 100.0,
                    "spent": 128.36,
                },
            },
        )
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(self.request.total_reimburseable, (Decimal(105), "USD"))
            self.assertTrue(self.request.over_budget)

    def test_reimbursement_lines(self):
        self.create_request()
        self.create_request_line()
        self.create_receipt_line(self.create_receipt())
        self.create_reimbursement(amount=Decimal(13))
        self.assertEqual(self.request.reimbursed_total, (Decimal(13), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 13.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 13.0,
                    "requested": 100.0,
                    "spent": 64.18,
                },
            },
        )

    def test_reimbursement_lines_across_types(self):
        self.create_request()
        self.create_request_line()
        receipt = self.create_receipt()
        self.create_receipt_line(receipt)
        trains = ExpenseType.objects.create(name="Train Tickets")
        self.create_receipt_line(receipt, expense_type=trains, amount=Decimal(10))
        self.create_reimbursement()
        self.create_reimbursement(expense_type=trains, amount=Decimal("10.88"))
        self.assertEqual(self.request.reimbursed_total, (Decimal("75.06"), "USD"))
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Accommodation": {
                    "currency": "USD",
                    "reimbursed": 64.18,
                    "requested": 100.0,
                    "spent": 64.18,
                },
                "Train Tickets": {
                    "currency": "USD",
                    "reimbursed": 10.88,
                    "requested": 0.0,
                    "spent": 10.88,
                },
                "Total": {
                    "currency": "USD",
                    "reimbursed": 75.06,
                    "requested": 100.0,
                    "spent": 75.06,
                },
            },
        )

    def test_pretty_bank_details(self):
        self.create_request()
        self.request.bank_details = {
            "wise_type": "iban",
            "recipient_name": "Testy Tester",
            "iban": "BE123",
        }
        rendered = self.request.pretty_bank_details
        self.assertIsInstance(rendered, dict)
        self.assertEqual(rendered["IBAN"], "BE123")
