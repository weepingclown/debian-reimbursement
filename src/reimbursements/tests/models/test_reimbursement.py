from decimal import Decimal

from django.test import SimpleTestCase

from reimbursements.models.reimbursement import ReimbursementCost, ReimbursementLine
from reimbursements.models.request import Request


class TestReimbursementLine(SimpleTestCase):
    def test_qualified_amount(self):
        line = ReimbursementLine(
            request=Request(currency="EUR"),
            amount=Decimal("19.95"),
        )
        self.assertEqual(line.qualified_amount, (Decimal("19.95"), "EUR"))


class TestReimbursementCost(SimpleTestCase):
    def test_qualified_amount(self):
        cost = ReimbursementCost(request=Request(), amount=Decimal(12), currency="USD")
        self.assertEqual(cost.qualified_amount, (Decimal(12), "USD"))
