from django.contrib.auth import get_user_model
from django.test import SimpleTestCase

from reimbursements.models.profile import Profile


class TestProfile(SimpleTestCase):
    def test_str(self):
        User = get_user_model()
        profile = Profile(user=User(username="user"), bank_details={})
        self.assertEqual(str(profile), "Profile for user")
