from csv import DictWriter
from datetime import datetime

from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from django.db.models import Q

from historical_currencies.exchange import exchange
from iso4217 import Currency

from reimbursements.models import Request, RequestHistory


def group_name(arg: str) -> Group:
    """Look up a Group by name"""
    try:
        return Group.objects.get_by_natural_key(arg)
    except Group.DoesNotExist as e:
        raise ValueError(e)


class Command(BaseCommand):
    help = "Summarize requests made in a given date range, in CSV"

    def add_arguments(self, parser):
        parser.add_argument(
            "--from",
            metavar="TIMESTAMP",
            type=datetime.fromisoformat,
            help="Starting timestamp for requests to review",
        )
        parser.add_argument(
            "--to",
            metavar="TIMESTAMP",
            type=datetime.fromisoformat,
            help="Ending timestamp for requests to review",
        )
        parser.add_argument(
            "--payer",
            metavar="NAME",
            type=group_name,
            help="Only review requests payable by NAME",
        )
        parser.add_argument(
            "--state",
            metavar="NAME",
            type=Request.States,
            help="Only review requests in STATE",
        )
        parser.add_argument(
            "--currency",
            metavar="CODE",
            type=Currency,
            help="Convert requests into currency CODE",
            default="USD",
        )

    def handle(self, *args, **options):
        query = Request.objects.filter()
        if options["from"]:
            query = query.filter(
                Q(
                    history__event=RequestHistory.Events.CREATED,
                    history__timestamp__ge=options["from"],
                )
            )
        if options["to"]:
            query = query.filter(
                Q(
                    history__event=RequestHistory.Events.CREATED,
                    history__timestamp__le=options["to"],
                )
            )
        if options["payer"]:
            query = query.filter(payer_group=options["payer"])
        if options["state"]:
            query = query.filter(state=options["state"])

        csv_writer = DictWriter(
            self.stdout,
            [
                "request_id",
                "state",
                "payer",
                "requested",
                "spent",
                "reimbursed",
            ],
        )
        csv_writer.writeheader()
        for request in query:
            display_currency = options["currency"].code
            total = request.total
            converted_total = exchange(total[0], total[1], display_currency)
            spent = request.receipts_total
            converted_spent = exchange(spent[0], spent[1], display_currency)
            reimbursed = request.reimbursed_total
            converted_reimbursed = exchange(
                reimbursed[0], reimbursed[1], display_currency
            )

            csv_writer.writerow(
                {
                    "request_id": request.id,
                    "state": request.state,
                    "payer": request.payer_group.name,
                    "requested": converted_total,
                    "spent": converted_spent,
                    "reimbursed": converted_reimbursed,
                }
            )
