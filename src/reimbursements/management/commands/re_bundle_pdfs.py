from django.core.management.base import BaseCommand

from reimbursements.models import Request


class Command(BaseCommand):
    help = "Regenerate request PDF Bundle"

    def add_arguments(self, parser):
        parser.add_argument(
            "--request",
            type=int,
            metavar="ID",
            help="Request to bundle PDF for. Default, all requests",
        )

    def handle(self, *args, **options):
        requests = Request.objects

        if options["request"]:
            requests = requests.filter(id=options["request"])
        else:
            requests = requests.all()

        for request in requests:
            print(f"Request {request.id}")
            request.generate_pdf_bundle()
