import datetime
from decimal import Decimal
from functools import lru_cache

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from historical_currencies.exceptions import ExchangeRateUnavailable
from historical_currencies.exchange import exchange

from reimbursements.models.receipt import Receipt
from reimbursements.models.request import Request
from reimbursements.models.types import ExpenseType


@lru_cache
def conversion_leeway_minimum(currency: str, date: datetime.date) -> Decimal:
    """Convert CONVERSION_LEEWAY_MINIMUM_USD to currency for date."""
    usd_minimum = Decimal(settings.CONVERSION_LEEWAY_MINIMUM_USD)
    return exchange(usd_minimum, "USD", currency, date)


class ReceiptLine(models.Model):
    receipt = models.ForeignKey(Receipt, on_delete=models.CASCADE, related_name="lines")
    date = models.DateField()
    description = models.TextField()
    amount = models.DecimalField(decimal_places=2, max_digits=15)
    currency = models.CharField(max_length=3)
    converted_amount = models.DecimalField(decimal_places=2, max_digits=15)
    expense_type = models.ForeignKey(ExpenseType, on_delete=models.PROTECT)

    @property
    def qualified_amount(self):
        return (self.amount, self.currency)

    @property
    def qualified_converted_amount(self):
        return (self.converted_amount, self.receipt.request.currency)

    @property
    def automatically_converted_amount(self):
        return exchange(
            self.amount, self.currency, self.receipt.request.currency, self.date
        )

    @property
    def qualified_automatically_converted_amount(self):
        return (self.automatically_converted_amount, self.receipt.request.currency)

    @property
    def conversion_within_limits(self):
        if not self.receipt.custom_conversion:
            return True
        request_currency = self.receipt.request.currency
        leeway_minimum = conversion_leeway_minimum(request_currency, self.date)
        auto_converted = self.automatically_converted_amount
        difference = self.converted_amount - auto_converted
        if difference < leeway_minimum:
            return True
        ratio = self.converted_amount / auto_converted
        leeway = Decimal(1 + settings.CONVERSION_LEEWAY_PERCENTAGE / 100)
        return ratio <= leeway

    def clean(self):
        super().clean()
        request = None
        currency_to = getattr(self, "initial_request_currency", None)
        try:
            request = self.receipt.request
            currency_to = request.currency
        except Receipt.DoesNotExist:
            pass
        except Request.DoesNotExist:
            pass
        if (
            self.amount
            and self.currency
            and currency_to
            and not self.receipt.custom_conversion
        ):
            try:
                self.converted_amount = exchange(
                    amount=self.amount,
                    currency_from=self.currency,
                    currency_to=currency_to,
                    date=self.date,
                )
            except ExchangeRateUnavailable:
                raise ValidationError(
                    {
                        "currency": _(
                            "No exchange rate available for %(currency_from)s "
                            "to %(currency_to)s for %(date)s. Please contact "
                            "an admin to get it imported."
                        )
                        % {
                            "currency_from": self.currency,
                            "currency_to": currency_to,
                            "date": self.date,
                        }
                    }
                )

        if (
            request
            and self.expense_type_id
            and not self.expense_type.request_types.filter(pk=request.request_type.pk)
        ):
            raise ValidationError(
                {
                    "expense_type": _("%s is not a valid expense type for %s requests")
                    % (self.expense_type.name, request.request_type.name),
                }
            )


@receiver(post_save, sender=Request)
def update_receipt_conversions(sender, instance, **kwargs):
    """Update converted_amounts. Backstop for things like edits in admin."""
    for receipt in instance.receipts.all():
        for line in receipt.lines.all():
            line.clean()
            line.save()
