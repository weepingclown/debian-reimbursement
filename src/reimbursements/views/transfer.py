from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.shortcuts import redirect
from django.views.generic.detail import DetailView

from reimbursements.lifecycle import (
    request_transfer_initiate,
    request_transfer_complete,
)
from reimbursements.models import Request, RequestTransfer
from reimbursements.views.generic import DetailActionView


class RequestTransferInitiateView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"
    template_name = "reimbursements/transfer_initiated.html"

    def post(self, request, *args, **kwargs):
        self.object = request = self.get_object()

        if request.state not in (Request.States.DRAFT, Request.States.APPROVED):
            raise PermissionDenied(
                "Requests can only be transferred up to the approval state."
            )
        if hasattr(request, "transfer"):
            request.transfer.delete()
            request.refresh_from_db()

        if not request.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only transfer your own requests")

        request_transfer_initiate(request, self.request.user)

        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class RequestTransferClaimView(LoginRequiredMixin, DetailView):
    model = Request
    context_object_name = "object"
    template_name = "reimbursements/transfer_claim.html"

    def get_object(self):
        try:
            return RequestTransfer.objects.get_by_token(
                token=self.kwargs["token"]
            ).request
        except RequestTransfer.DoesNotExist:
            raise Http404("Transfer does not exist")

    def post(self, request, *args, **kwargs):
        self.object = request = self.get_object()

        if request.state not in (Request.States.DRAFT, Request.States.APPROVED):
            raise PermissionDenied(
                "Requests can only be transferred up to the approval state."
            )

        if request.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("You cannot transfer to yourself")

        request_transfer_complete(request, self.request.user)

        return redirect(request.get_absolute_url())
