from typing import Any

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import FileResponse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.generic.detail import DetailView

from image_wrangler.authentication import LoginOrAccessKeyRequiredMixin
from reimbursements.forms.receipt import ReceiptForm
from reimbursements.forms.receiptline import (
    ReceiptLineEditFormset,
    ReceiptLineFormHelper,
    ReceiptLineFormset,
)
from reimbursements.lifecycle import (
    receipt_created,
    receipt_deleted,
    receipt_updated,
)
from reimbursements.models import (
    Receipt,
    Request,
)
from reimbursements.views.generic import (
    DetailActionView,
    FormAndSetCreateView,
    FormAndSetUpdateView,
)


class ReceiptChildView:
    template_name: str | None = "reimbursements/edit_receipt.html"
    # FIXME: find the correct type annotation
    form_class: Any = ReceiptForm
    formset_class = ReceiptLineFormset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["formset_helper"] = ReceiptLineFormHelper()
        context["request_currency"] = self.request_model.currency
        # TODO: We could limit the options in the select box, server-side. But
        # this means passing context more through to the form instance.
        context["valid_expense_types"] = [
            expense_type.id
            for expense_type in self.request_model.request_type.expense_types.all()
        ]
        return context

    def get_request(self):
        self.request_model = Request.objects.get(id=self.request_id)
        if not self.request_model.user_access(self.request.user).get(
            "requester", False
        ):
            raise PermissionDenied("Can only edit your own requests")
        if self.request_model.state != Request.States.APPROVED:
            raise PermissionDenied("Requests need to be approved to edit receipts")

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        form.request = self.request_model
        return form

    def get_formset(self, formset_class, instance=None):
        formset = super().get_formset(formset_class, instance=instance)
        formset.default_currency = self.request_model.currency
        return formset

    def get(self, request, *args, **kwargs):
        self.request_id = kwargs["request_id"]
        self.get_request()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.request_id = kwargs["request_id"]
        self.get_request()
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return redirect(self.request_model.get_absolute_url())


class ReceiptCreateView(ReceiptChildView, LoginRequiredMixin, FormAndSetCreateView):
    def is_valid(self, form, formset):
        for subform in formset.forms:
            # During validation of new object creation, we can't access the
            # receipt's request object. Smuggle it in.
            subform.instance.initial_request_currency = self.request_model.currency
        return super().is_valid(form, formset)

    def forms_valid(self, form, formset):
        with transaction.atomic():
            r = super().forms_valid(form, formset)
            receipt_created(receipt=self.object, actor=self.request.user)
        return r


class ReceiptEditView(ReceiptChildView, LoginRequiredMixin, FormAndSetUpdateView):
    formset_class = ReceiptLineEditFormset
    model = Receipt

    def forms_valid(self, form, formset):
        with transaction.atomic():
            r = super().forms_valid(form, formset)
            receipt_updated(receipt=self.object, actor=self.request.user)
        return r


class ReceiptDeleteView(LoginRequiredMixin, DetailActionView):
    model = Receipt

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        request = self.object.request
        if not request.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only delete receipts for your own requests")
        if self.object.request.state != Request.States.APPROVED:
            raise PermissionDenied("Requests need to be approved to edit receipts")
        self.object.delete()
        receipt_deleted(receipt=self.object, actor=self.request.user)
        return redirect(request.get_absolute_url())


@method_decorator(xframe_options_sameorigin, name="dispatch")
class ReceiptFileView(LoginRequiredMixin, DetailView):
    model = Receipt

    def render_to_response(self, context):
        if not self.object.request.user_access(self.request.user):
            raise PermissionDenied()
        return FileResponse(self.object.file)


@method_decorator(xframe_options_sameorigin, name="dispatch")
class ReceiptThumbnailView(LoginOrAccessKeyRequiredMixin, DetailView):
    model = Receipt

    def render_to_response(self, context):
        if not self.object.request.user_access(
            self.request.user, self.request.valid_access_key
        ):
            raise PermissionDenied()
        return FileResponse(self.object.thumbnail)


class RequestReceiptsView(LoginRequiredMixin, DetailView):
    model = Request
    template_name = "reimbursements/request_receipts.html"
    context_object_name = "object"

    def get_context_data(self, **kwargs):
        access = self.object.user_access(self.request.user)
        if not access:
            raise PermissionDenied()
        context = super().get_context_data(**kwargs)
        context["access"] = access
        return context
