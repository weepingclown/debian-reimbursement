from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper

from reimbursements.models import Request, RequestLine


class RequestLineForm(forms.ModelForm):
    class Meta:
        model = RequestLine
        widgets = {
            "description": forms.TextInput(),
        }
        exclude = ()


class RequestLineFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_tag = False
        self.template = f"{settings.CRISPY_TEMPLATE_PACK}/table_inline_formset.html"


class RequestLineInlineFormset(forms.models.BaseInlineFormSet):
    def clean(self):
        count = sum(
            1
            for form in self.forms
            if getattr(form, "cleaned_data", None)
            and not form.cleaned_data.get("DELETE", False)
        )
        if count < 1:
            raise ValidationError(_("At least one request line is required."))
        return super().clean()


RequestLineFormset = forms.models.inlineformset_factory(
    Request,
    RequestLine,
    form=RequestLineForm,
    formset=RequestLineInlineFormset,
    exclude=(),
    extra=1,
)
