from django import forms
from django.conf import settings
from django.contrib.auth.models import Group
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper
from historical_currencies.choices import currency_choices

from reimbursements.models import Request
from reimbursements.widgets import NativeDateInput


class RequestForm(forms.ModelForm):
    class Meta:
        model = Request
        fields = ("request_type", "description", "expected_date", "currency")
        widgets = {
            "currency": forms.Select(choices=currency_choices()),
            "expected_date": NativeDateInput(),
        }
        help_texts = {
            "description": _(
                "Describe the request in a sentence or two. "
                "First line is the subject (like a git commit)."
            ),
            "expected_date": _(
                "(optional) Date that you expect to claim reimbursement."
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    def save(self, commit=True):
        instance = super().save(commit=False)
        if instance._state.adding:
            instance.approver_group = Group.objects.get(name=settings.DEFAULT_APPROVER)
            payer_selector = import_string(settings.PAYER_SELECTOR)
            instance.payer_group = payer_selector(instance)
            instance.requester = self.request_user
        if commit:
            instance.save()
        return instance
