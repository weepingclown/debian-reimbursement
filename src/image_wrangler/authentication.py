from typing import Protocol

from django.contrib.auth.mixins import AccessMixin
from django.core.signing import BadSignature, SignatureExpired, TimestampSigner
from django.http import HttpRequest

MSG = "image_wrangler.access"


class ViewProtocol(Protocol):
    request: HttpRequest


def _signer():
    """Create a TimestampSigner that can be used in Authorization headers"""
    # This is the only character defined in b64token in RFC6750 that isn't used
    # by Base 64 itself (and therefore considered unsafe by Django)
    return TimestampSigner(sep="~")


def generate_access_key():
    """Generate an access token."""
    return _signer().sign(MSG)


def validate_access_key(access_key: str, expiry: int = 60) -> bool:
    """
    Validate that access_key is valid and not greater than expiry seconds old.
    """
    try:
        msg = _signer().unsign(access_key, max_age=expiry)
    except (BadSignature, SignatureExpired):
        return False
    if msg != MSG:
        return False
    return True


class AccessKeyAcceptedMixin:
    def valid_access_key(self: ViewProtocol) -> bool:
        auth_header = self.request.headers.get("Authorization", None)
        if not auth_header:
            return False
        if not auth_header.startswith("Bearer "):
            return False
        key = auth_header.split(None, 1)[1]
        return validate_access_key(key)


class LoginOrAccessKeyRequiredMixin(AccessKeyAcceptedMixin, AccessMixin):
    """Replacement for LoginRequiredMixin that accepts Access Keys too."""

    def dispatch(self, request, *args, **kwargs):
        request.valid_access_key = self.valid_access_key()
        if not request.valid_access_key and not request.user.is_authenticated:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
