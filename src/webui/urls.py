from django.urls import path

from webui.views import DeleteBankDetailsView, IndexView, ProfileEditView, ProfileView

urlpatterns = [
    path("", IndexView.as_view()),
    path("profile", ProfileView.as_view(), name="profile"),
    path("profile/edit", ProfileEditView.as_view(), name="profile-edit"),
    path(
        "profile/delete-bank-details",
        DeleteBankDetailsView.as_view(),
        name="profile-delete-bank-details",
    ),
]
