from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def approver_groups(context):
    """List the approver groups that the user is a member of."""
    groups = {}
    for group in context.request.user.groups.all():
        if group.requests_approver.exists():
            groups[group.id] = group.name
    return groups


@register.simple_tag(takes_context=True)
def payer_groups(context):
    """List the payer groups that the user is a member of."""
    groups = {}
    for group in context.request.user.groups.all():
        if group.requests_payer.exists():
            groups[group.id] = group.name
    return groups
