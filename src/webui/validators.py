from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


def no_starting_dot(username):
    """Avoid unix hidden filenames"""
    if username.startswith("."):
        raise ValidationError(_('usernames cannot start with a "."'))


def not_index(username):
    """Avoid problematic static file names"""
    if username in ("index.html",):
        raise ValidationError(_("This username is not available"))


username_validators = [
    no_starting_dot,
    not_index,
]
