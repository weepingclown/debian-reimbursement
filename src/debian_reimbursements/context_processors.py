from django.conf import settings


def meta(request):
    """Expose useful settings in request context"""
    return {
        "SITE_NAME": settings.SITE_NAME,
        "SITE_PROD_STATE": settings.SITE_PROD_STATE,
        "SITE_URL": settings.SITE_URL,
    }
