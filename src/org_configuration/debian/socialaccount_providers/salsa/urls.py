from allauth.socialaccount.providers.oauth2.urls import (
    default_urlpatterns,
)

from org_configuration.debian.socialaccount_providers.salsa.provider import (
    SalsaProvider,
)

urlpatterns = default_urlpatterns(SalsaProvider)
