from allauth.socialaccount.providers.gitlab.views import (
    GitLabOAuth2Adapter,
)
from allauth.socialaccount.providers.oauth2.views import (
    OAuth2CallbackView,
    OAuth2LoginView,
)


class SalsaOAuth2Adapter(GitLabOAuth2Adapter):
    provider_id = "salsa"
    provider_default_url = "https://salsa.debian.org/"


oauth2_login = OAuth2LoginView.adapter_view(SalsaOAuth2Adapter)
oauth2_callback = OAuth2CallbackView.adapter_view(SalsaOAuth2Adapter)
